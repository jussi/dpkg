(ns main
  (:require [reagent.dom :as dom]
            [re-frame.core :as re-frame]
            [reitit.frontend.easy :as rfe]
            [routing]
            [day8.re-frame.http-fx]
            [routes :refer [routes]]))

(defn current-route
  []
  (when-let [route-match @(re-frame/subscribe [::routing/match])]
    (if-let [view-fn (get-in route-match [:data :view])]
      [view-fn (:params view-fn)]
      [:p "No view function defined for route."])))

(defn main-view
  []
  [:div.main-container
   [current-route]])

(defn ^:dev/after-load render
  []
  (re-frame/clear-subscription-cache!)
  (dom/render [main-view] (.getElementById js/document "app")))

(defn ^:export init
  []
  (rfe/start! (routes)
              #(re-frame/dispatch [::routing/navigate %])
              {:use-fragment false})
  (render))
