(ns components.nav-bar.nav-bar)

(defn nav-bar
  []
  [:nav.nav-bar
   [:a.nav-bar__app-title {:href  "/"} "dpkg"]
   [:a.nav-bar__link {:href "/"} "list"]])
