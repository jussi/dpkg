(ns pages.dpkg.subs
  (:require [re-frame.core :as re]))

(def +path+
  [:data :dpkg])

(def +fetch-state+
  (concat +path+ [:state]))

(def +packages+
  (concat +path+ [:packages]))

(def +selected+
  (concat +path+ [:selected]))

(def +registry+
  (concat +packages+ [:registry]))

(def +transitive+
  (concat +packages+ [:transitive]))

(def +reverse+
  (concat +packages+ [:reverse]))

;;
;; subs
;;

(re/reg-sub
  ::state
  (fn [db _]
    (get-in db +fetch-state+)))

(re/reg-sub
  ::packages
  (fn [db _]
    (get-in db +packages+)))

(re/reg-sub
  ::registry
  (fn [db _]
    (get-in db +registry+)))

(re/reg-sub
  ::transitive
  (fn [db _]
    (get-in db +transitive+)))

(re/reg-sub
  ::reverse
  (fn [db _]
    (get-in db +reverse+)))

(re/reg-sub
  ::selected-package-name
  (fn [db _]
    (get-in db +selected+)))

(re/reg-sub
  ::selected-package
  :<- [::registry]
  :<- [::transitive]
  :<- [::reverse]
  :<- [::selected-package-name]
  (fn [[registry transitive reverse selected] _]
    (let [package (get registry selected)]
      (assoc package
        :name selected
        :package (get registry selected)
        :transitive (get transitive selected)
        :reverse (get reverse selected)))))
