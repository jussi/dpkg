(ns pages.dpkg.events
  (:require [re-frame.core :as re]
            [ajax.core :as ajax]
            [pages.dpkg.subs :as subs]))

(defn mk-execution-list-uri
  []
  (str "/api/dpkg"))

(re/reg-event-fx
  ::reset-dpkg-list
  (fn reset-dpkg-list
    [{:keys [db] :as _ctx} [_ selected-package]]
    (let [packages (get-in db subs/+registry+)]
      (cond-> {:db (-> db (assoc-in subs/+selected+ selected-package))}
              (nil? packages) (assoc-in (into [:db] subs/+fetch-state+) :loading)
              (nil? packages) (assoc
                                  :http-xhrio {:method          :get
                                               :uri             (mk-execution-list-uri)
                                               :timeout         5000
                                               :response-format (ajax/transit-response-format)
                                               :on-success      [::get-dpkg-list-success]
                                               :on-failure      [::get-dpkg-list-failure]})))))

(re/reg-event-db
  ::get-dpkg-list-success
  (fn get-dpkg-list-success
    [db [_ data]]
    (-> db
        (update-in subs/+path+ assoc :packages data)
        (assoc-in subs/+fetch-state+ :ready))))

(re/reg-event-db
  ::get-dpkg-list-failure
  (fn get-dpkg-list-failure
    [db _]
    (assoc-in db subs/+fetch-state+ :error)))
