(ns pages.dpkg.view
  (:require [re-frame.core :as re]
            [pages.dpkg.events :as events]
            [pages.dpkg.subs :as subs]
            [components.nav-bar.nav-bar :refer [nav-bar]]))

(defn- view-link
  ([package]
   [view-link nil package])
  ([class package]
   [:a (cond-> {:href (str "/view/" package)}
               class (assoc :class class))
    package]))

(defn render-dependency-list
  [deps]
  [:ul.dpkg__packages-deps
   (for [dependency deps]
     ^{:key dependency}
     [:li.dpkg__packages-deps-item
      [view-link "dpkg__packages-deps-link" dependency]])])

(defn render-dependency
  [title deps]
  (when (seq deps)
    ^{:key title}
    [:<>
     [:h2 title]
     [render-dependency-list deps]]))

(defn render-dependency-lists
  [dependencies reverse transitive]
  [:div
   [render-dependency "Dependencies" dependencies]
   [render-dependency "Reverse dependencies" reverse]
   [render-dependency "Transitive dependencies" transitive]])

(defn render-packages
  [packages]
  [:div.dpkg__packages-container
   [:h1 "Packages"]
   [:table.dpkg__packages-list
    [:tbody
     (for [package (into (sorted-map) packages)
           :let [package-name (key package)]]
       ^{:key package-name}
       [:tr
        [:td
         [view-link package-name]]])]]])

(defn render-package
  [{:keys [name description depends reverse transitive]}]
  [:div.dpkg__packages-container
   [:h1 name]
   [:pre description]
   (when (or depends reverse transitive)
     [render-dependency-lists depends reverse transitive])])

(defn layout
  [state f]
  [:div.dpkg-page
   [nav-bar]
   [:div.dpkg-container
    (if (= state :ready)
      f
      [:div "Ladataan"])]])

;; views

(defn dpkg-list
  []
  (let [registry (re/subscribe [::subs/registry])
        state (re/subscribe [::subs/state])]
    (fn []
      [layout @state [render-packages @registry]])))

(defn dpkg-view
  []
  (let [selected (re/subscribe [::subs/selected-package])
        state (re/subscribe [::subs/state])]
    (fn []
      [layout @state [render-package @selected]])))

(def list-packages {:name ::dpkg-list
                    :view dpkg-list
                    :controllers [{:start #(re/dispatch [::events/reset-dpkg-list])}]})

(def view-package {:name        ::dpkg-view
                   :view        dpkg-view
                   :parameters  {:path {:package string?}}
                   :controllers [{:parameters {:path [:package]}
                                  :start (fn [{:keys [path]}]
                                           (re/dispatch [::events/reset-dpkg-list (:package path)]))}]})
