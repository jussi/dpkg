(ns routing
  (:require [reitit.frontend.controllers :as rfc]
            [re-frame.core :as re]))

(re/reg-event-fx
 :routing/navigate
 (fn [db [_ new-match]]
   (let [old-match (get db :routing/match)
         controllers (:controllers old-match)]
     {:db (merge (:db db)
                 (->>
                   (rfc/apply-controllers controllers new-match)
                   (assoc new-match :controllers)
                   (assoc (:db db) :routing/match)))})))

(re/reg-sub :routing/match
            (fn [db _]
              (:routing/match db)))
