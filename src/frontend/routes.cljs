(ns routes
  (:require [reitit.frontend :as rf]
            [pages.dpkg.view :as dpkg]))

(defn routes
  []
  (rf/router
   ["/"
    ["" dpkg/list-packages]
    ["view/:package" dpkg/view-package]
    ["foo" {:name :sub-page
            :view (constantly [:h2 "Sub page"])}]]))
