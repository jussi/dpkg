(ns dpkg.core
  (:require [integrant.core :as ig]
            [camel-snake-kebab.core :as csk]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.tools.logging :as log]))

(def ^:private +included-package-fields+
  [:package :description :depends])

(defn text-line?
  [line]
  (str/starts-with? line " "))

(defn parse-alternatives
  [packages]
  (mapcat #(str/split % #"\|") packages))

(defn parse-depends
  "Parse `depends` field, omit version info."
  [value]
  (if-let [depends (some-> value (str/split #",") parse-alternatives)]
    (reduce
      (fn [acc package-name-with-version]
        (conj acc (some-> package-name-with-version
                          (str/trim)
                          (str/split #" ")
                          first)))
      #{}
      depends)))

(defn parse-package-data
  "Parse package fields, currently only parses :depends."
  [package-data]
  (when package-data
    (let [data (->> (select-keys package-data +included-package-fields+)
                    (map (fn [[key value]]
                           ;; Cast vectors into strings
                           (let [value (some->> value (apply str))]
                             (case key
                               :depends {key (parse-depends value)}
                               {key value}))))
                    (into {}))]
      {(:package data) (dissoc data :package)})))

(defn format-text-line
  "Add formatting to text-only lines"
  [data]
  (when-let [text (some-> data (str/trim))]
    (if (= text ".")
      "\n"
      (format "\n%s" text))))

(defn parse-packages
  "Parse packages in a sequence"
  [coll]
  (let [parsed-data (atom [])
        ;; used when multiple lines are being appended
        previous-key (atom nil)
        current-package (atom {})]
    (doseq [data coll]
      ;; Expects to get an empty line when the package changes
      (if-not (seq data)
        ;; Store the parsed package
        (do (swap! parsed-data conj (parse-package-data @current-package))
            (reset! current-package {}))
        (if (text-line? data)
          ;; If current data belongs to the previous entry
          ;; conjoin it with previous key.
          (swap! current-package update @previous-key conj (format-text-line data))

          ;; Assoc `key: value` into `current-package` atom
          (let [split-line-data (str/split data #": " 2)
                [key value] split-line-data
                key (csk/->kebab-case-keyword key)]
            (reset! previous-key key)
            (swap! current-package assoc key [value])))))
    @parsed-data))

(defn resolve-reverse-dependencies
  "Resolve reverse dependencies for packages"
  [registry]
  (let [;; packages without any dependencies
        ready (filter #(empty? (-> % val :depends)) registry)
        ;; packages with dependencies
        to-be-resolved (apply dissoc registry (keys ready))
        ;; add all keys from registry to `updated-deps`
        updated-deps (atom (zipmap (keys registry) (repeat [])))]
    (doseq [;; Iterate all in `to-be-resolved`
            package to-be-resolved
            ;; Iterate each `depends`
            dependency (-> package val :depends)
            ;; Package name is the reverse dependency of `dependency`
            :let [package-name (-> package key)]]
      (swap!
        updated-deps
        update
        dependency
        (fn [deps package-name]
          (if (seq deps)
            (conj deps package-name)
            [package-name]))
        package-name))
    (into {} @updated-deps)))

(defn get-transitive-dependencies
  "Get transitive dependencies for a package"
  ([registry package]
   (get-transitive-dependencies registry package #{} #{}))
  ([registry package resolved visited]
   (if-let [package-dependencies (->> (:depends (get registry package)))]
     (->> package-dependencies
          (remove #(.contains visited %))
          (mapcat (fn [dependency]
                    (get-transitive-dependencies
                      registry
                      dependency
                      (set (conj resolved dependency))
                      (set (conj visited package)))))
          (into #{}))
     (set resolved))))

(defn resolve-transitive-dependencies
  "Resolve transitive dependencies for all packages"
  [registry]
  (let [get-transitive-dependencies'
        (partial get-transitive-dependencies registry)]
    (->> (for [package registry
               :let [package-name (key package)
                     values (-> package val)
                     depends (-> values :depends)]]
           {package-name (set (mapcat get-transitive-dependencies' depends))})
         (into {}))))

(defn lazy-load-file
  "Separate IO from processing"
  [f filename]
  (with-open [reader (some-> filename (io/resource) (io/reader))]
    (->> (line-seq reader)
         (f))))

;;
;; Public api
;;

(def load-dpkg-file
  "Parses given dpkg file"
  (partial lazy-load-file parse-packages))

(defmethod ig/init-key :component/import-dpkg-file
  [_ {:keys [filename] :as _config}]
  (log/infof "Parsing dpkg information from %s" filename)
  (let [packages (->> (load-dpkg-file filename)
                      (into {}))
        reverse (resolve-reverse-dependencies packages)
        transitive (resolve-transitive-dependencies packages)]
    (log/infof "Done parsing")
    {:registry packages
     :reverse reverse
     :transitive transitive}))

(comment
  (->> (load-dpkg-file "dpkg.stuff")
       (into {})))

