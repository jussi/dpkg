(ns system
  (:require [http.handler]
            [http.server]

            ;; parse dpkg file on startup
            [dpkg.core]))
