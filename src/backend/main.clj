(ns main
  (:gen-class)
  (:require [integrant.core :as ig]
            [clojure.tools.logging :as log]
            [config]
            [system]))

(defonce system nil)

(defn reset-system
  [prev-system]
  (when prev-system
    (ig/halt! prev-system))
  (ig/init (config/get-system)))

(defn -main
  [& _]
  (try
    (alter-var-root #'system reset-system)
    (catch Throwable t
      (log/fatal t "Failed to start system"))))
