(ns util.dotenv
  "Extends System/getenv with properties and .env-file"
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.tools.logging :as log]))

(def ^:private +dot-env-filename+ ".env")

;;
;; Utilities
;;

(defn- split-env-vars
  [col]
  (when col
    (let [[key env-val] (str/split col #"\=" 2)]
      (when (and key env-val)
        [key (str/trim env-val)]))))

(defn- slurp-resource
  [resource-file-name]
  (try (when-let [file-handle
                  (or (some-> resource-file-name (io/resource) (io/file))
                      (some-> resource-file-name (io/as-file)))]
         (slurp file-handle))
       (catch Exception _e
         (log/info ".env file not found"))))

(defn load-dot-env-file
  ([]
   (load-dot-env-file +dot-env-filename+))
  ([dot-env-filename]
   (some->> dot-env-filename
            slurp-resource
            str/split-lines
            (map split-env-vars)
            (remove nil?))))

;;
;; Public api
;;

(defn dotenv
  "Read .env file from resources"
  ([]
   (dotenv +dot-env-filename+))
  ([file-name]
   (let [content (load-dot-env-file file-name)]
     (into {} content))))

(defn with-dotenv
  "Merge dotenv with system environment variables and properties."
  [filenames]
  (apply merge {} (-> (for [f filenames] (dotenv f))
                      (conj (System/getenv))
                      (conj (System/getProperties)))))

(defn env-with-files
  "Get environment variables extended with default and additional dotenv files."
  [filenames]
  (fn -env-with-files
    ([]
     (-env-with-files nil))
    ([key]
     (cond-> (with-dotenv (cons +dot-env-filename+ filenames))
       key (get key)))))

(def env
  "Get key environment variables extended with those in default dotenv file.
  System environment variables always take precedence over dotenv variables."
  (env-with-files []))
