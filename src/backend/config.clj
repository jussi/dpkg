(ns config
  (:require [aero.core :refer [read-config] :as aero]
            [integrant.core :as ig]
            [clojure.java.io :as io]
            [util.dotenv :refer [env env-with-files]]))

(def ^:private config-file "config.edn")

(defmethod aero/reader 'ig
  [_ _ value]
  (ig/ref value))

(defmethod aero/reader 'env
  [opts _tag value]
  (if-let [dotenv-files (:dotenv-files opts)]
    ((env-with-files dotenv-files) (str value))
    (env (str value))))

(defn load-config
  ([]
   (load-config nil))
  ([opts]
   (read-config (io/resource config-file) opts)))

(defn get-system
  []
  (:system (load-config)))
