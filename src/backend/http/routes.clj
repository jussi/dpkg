(ns http.routes
  (:require [clojure.spec.alpha :as s]
            [ring.util.http-response :refer [ok]]))

(defn get-routes
  [{:keys [dpkg] :as _system}]
  [["/dpkg" {:get {:handler (fn [_]
                              (ok dpkg))}}]])
