(ns http.handler
  (:require [integrant.core :as ig]
            [reitit.ring :as reitit]
            [reitit.ring.spec :as rrs]
            [reitit.coercion.spec]
            [muuntaja.core :as muuntaja]
            [http.index :refer [mk-index-handler]]
            [http.middleware :refer [middleware]]
            [http.routes :refer [get-routes]]
            [http.static-files :refer [mk-static-handler]]
            [ring.logger :as logger]
            [ring.util.http-response :refer [ok]]
            [util.dotenv :refer [env]]))

(defmethod ig/init-key :component/api-handler
  [_ {:keys [_dpkg] :as config}]
  (reitit/ring-handler
   (reitit/router
    [["/healthz" {:get {:handler (fn [_]
                                   (ok))}}]
     ["/api" (get-routes config)]]
    {:data {:coercion reitit.coercion.spec/coercion
            :muuntaja muuntaja/instance
            :middleware middleware}
     :validate rrs/validate})
   (some-fn
    (mk-static-handler)
    (reitit/create-default-handler {:not-found (mk-index-handler)
                                    :middleware [logger/wrap-with-logger]}))))
