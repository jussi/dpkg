(ns http.static-files
  (:require [ring.middleware.content-type :as content-type]
            [ring.util.http-response :as resp]
            [util.dotenv :refer [env]]
            [clojure.tools.logging :as log]))

(defn load-resource-response
  [req]
  (try
    (resp/resource-response (:uri req)
                            {:root "public"})
    (catch Exception e
      (log/warn (.getMessage e))
      false)))

(defn load-file-response
  [req]
  (try
    (resp/file-response (:uri req)
                        {:root "target/dev/resources/public"})
    (catch Exception e
      (log/warn (.getMessage e))
      false)))

(defn mk-static-handler []
  (-> (fn [req]
        (when (= (:request-method req) :get)
          (or (load-file-response req)
              (load-resource-response req))))
      (content-type/wrap-content-type)))
