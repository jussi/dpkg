(ns http.middleware
  (:require [reitit.ring.middleware.parameters :as parameters]
            [reitit.ring.middleware.muuntaja :as muuntaja]
            [reitit.ring.middleware.exception :as exception]
            [ring.middleware.nested-params :as nested-params]
            [reitit.ring.coercion :as coercion]
            [ring.logger :as logger]
            [clojure.tools.logging :as log]))

(def wrap-nested-params
  "Middleware that converts flat parameters into a nested map.
   Differs from ring.middleware.nested-params/wrap-nested-params by
   modifying `:query-params` in addition to `:params`. This works with"
  {:name ::wrap-nested-params
   :wrap (fn -wrap-nested-params
           [handler]
           (fn [request]
             (-> request
                 (update :params #'nested-params/nest-params nested-params/parse-nested-keys)
                 (update :query-params #'nested-params/nest-params nested-params/parse-nested-keys)
                 handler)))})

(defn wrap-logging [level handler]
  (fn [^Throwable e {:keys [uri request-method] :as req}]
    (log/log level e (str request-method " " (pr-str uri) " => " (.getMessage e)))
    (handler e req)))

(def exception-middleware
  (exception/create-exception-middleware
   (merge exception/default-handlers
          {::exception/default (wrap-logging :error exception/default-handler)
           ::coercion/request-coercion (wrap-logging :warn (exception/create-coercion-handler 400))
           ::coercion/response-coercion (wrap-logging :error (exception/create-coercion-handler 500))})))

(def middleware
  [;; Request logging
   logger/wrap-with-logger
   ;; query-params & form-params
   parameters/parameters-middleware
   ;; convert single-depth param map to nested map
   wrap-nested-params
   ;; content-negotiation
   muuntaja/format-negotiate-middleware
   ;; encoding response body
   muuntaja/format-response-middleware
   ;; exception handling
   exception-middleware
   ;; decoding request body
   muuntaja/format-request-middleware
   ;; coercing response bodys
   coercion/coerce-response-middleware
   ;; coercing request parameters
   coercion/coerce-request-middleware])

