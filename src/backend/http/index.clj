(ns http.index
  (:require [hiccup.page :refer [html5]]
            [ring.util.http-response :refer [ok]]
            [ring.util.response :as response]
            [metosin.ring.util.hash :as hash]
            [metosin.ring.util.cache :as cache]))

(defn mk-init-script
  []
  "window.onload=function(){main.init();};")

(defn mk-index-handler
  []
  (let [init-script (mk-init-script)]
    (fn index-handler [_]
      (-> (html5
           {:lang "en"}
           [:head
            [:title ""]
            [:meta {:charset "UTF-8"}]
            [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
            [:link {:rel "stylesheet" :href (str "/css/main.css?v=" (hash/resource-hash "public/css/main.css"))}]]
           [:body
            [:div#app]
            [:script {:src (str "/js/main.js?v=" (hash/resource-hash "public/js/main.js")) :type "text/javascript"}]
            [:script {:type "text/javascript"} init-script]])
          (ok)
          (response/content-type "text/html; charset=UTF-8")
          (cache/cache-control cache/no-cache)))))
