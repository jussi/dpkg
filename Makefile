PACKAGE := app

.PHONY: backend
backend:
	mkdir -p target && DPKG_FILE=status.real clojure -A:common:backend:repl -J-XX:-OmitStackTraceInFastThrow

.PHONY: frontend
frontend: deps
	clojure -A:common:frontend watch frontend

.PHONY: css
css: deps
	clojure -Acss --auto

.PHONY: deps
deps:
	npm install

.PHONY: test
test:
	mkdir -p target && DPKG_FILE=status.test.real clojure -A:common:backend:test-clj -m kaocha.runner

.PHONY: lint
lint:
	clojure -Aformat-check
	clojure -Aclj-kondo

.PHONY: deps-outdated
deps-outdated:
	clojure -A:outdated

.PHONY: clean
clean:
	rm -rf node_modules/
	rm -rf target/

