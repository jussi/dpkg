(ns fixture.system
  (:require [config :refer [load-config]]
            [clojure.tools.logging :as log]
            [integrant.core :as ig]
            [dpkg.core]
            [http.handler]
            [http.server]))

(def ^:dynamic *test-system* nil)

(defn mk-system-fixture
  [mk-config]
  (fn [test-fn]
    (try
      (let [test-system (ig/init (mk-config))]
        (try
          (binding [*test-system* test-system]
            (test-fn))
          (catch Exception e
            (log/error e "Exception while running tests.")
            (throw e))
          (finally
            (when (map? test-system)
              (ig/halt! test-system)))))
      (catch Exception e
        (log/error e "Failed to construct test system.")
        (throw e)))))

(defn get-api
  ([]
   (get-api *test-system*))
  ([system]
   (:component/api-handler system)))

(defn- system
  []
  (let [config (load-config {:dotenv-files [".env.test"]})]
    (:system config)))

(defn test-system-fixture []
  (mk-system-fixture system))
