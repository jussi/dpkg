(ns dpkg.dpkg-test
  (:require [clojure.test :refer [use-fixtures deftest testing is are]]
            [fixture.system :refer [test-system-fixture]]
            [clojure.java.io :as io]
            [dpkg.core :refer [format-text-line
                               parse-packages
                               parse-depends
                               parse-package-data
                               resolve-reverse-dependencies]]))

(use-fixtures :each (test-system-fixture))

(def expected-parsed-file
  [{"libnet-ssleay-perl" {:description "foo", :depends #{"perl"}}}
   {"perl" {:description "foo", :depends #{"perl-base" "circular"}}}
   {"perl-base" {:description "foo"}}
   {"circular" {:description "foo", :depends #{"perl"}}}])

(deftest dpkg-core-test
  (testing "format-text-line"
    (is (= "\n" (format-text-line ".")))
    (is (= "\nFoobar." (format-text-line "Foobar.")))
    (is (= "\nFoobar\n." (format-text-line "Foobar\n."))))

  (testing "parse-depends"
    (let [depends "libbz2-1.0, libc6 (>= 2.14), libc6 (<= 3.14), libselinux1 (>= 1.32), zlib1g (>= 1:1.1.4), coreutils (>= 5.93-1), tar (>= 1.23), xz-utils"
          expected #{"libselinux1" "coreutils" "tar" "zlib1g" "libbz2-1.0" "libc6" "xz-utils"}
          result  (parse-depends depends)]
      (is (= expected result))))

  (testing "parse-package-data"
    (let [data {:package "foobar"
                :author "Nakkikukka"
                :origin "Debian"
                :description "foo"
                :depends ["base-files,make,make"]}
          expected {"foobar" {:description "foo"
                              :depends #{"base-files" "make"}}}]
      (is (= expected (parse-package-data data)))
      (is (= nil (parse-package-data nil)))))

  (testing "parse-packages"
    (let [test-file (some-> "status.test"
                            (io/resource)
                            (io/reader)
                            (line-seq))]
      (is (= expected-parsed-file (parse-packages test-file))))))



