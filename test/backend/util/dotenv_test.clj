(ns util.dotenv-test
  (:require [clojure.test :refer [use-fixtures deftest testing is]]
            [fixture.system :refer [test-system-fixture]]
            [util.dotenv :as dotenv]
            [clojure.java.io :as io]))

(use-fixtures :each (test-system-fixture))

(deftest dotenv-test
  (testing "reads env file"
    (let [dot-env-file (dotenv/load-dot-env-file "test.env")]
      (is (= dot-env-file '(["FOO" "bar"])))
      (is (= {"FOO" "bar"} (dotenv/dotenv "test.env"))))))
