(ns api.api-test
  (:require [clojure.test :refer [use-fixtures deftest testing is are]]
            [fixture.system :refer [test-system-fixture] :as fixture]
            [ring.mock.request :as mock]
            [jsonista.core :as jsonista]))

(use-fixtures :each (test-system-fixture))

(defn- parse-json-response
  [resp]
  (-> resp
      :body
      slurp
      (jsonista/read-value (jsonista/object-mapper {:decode-key-fn true}))))

(defn- mk-uri
  []
  (str "/api/dpkg"))

(def expected-api-payload
  {:transitive
   {:dpkg-dev [], :dpkg [], :python-minimal ["kissapaketti"]},
   :registry
   {:dpkg-dev
    {:description
     "Debian package development tools\nThis package provides the development tools (including dpkg-source)\nrequired to unpack, build and upload Debian source packages.",
     :depends
     ["base-files"
      "make"
      "patch"
      "binutils"
      "bzip2"
      "libdpkg-perl"
      "xz-utils"]},
    :dpkg
    {:description "Debian package management system\nThis package provides the low-level infrastructure for handling the\ninstallation and removal of Debian software packages.\n\nFor Debian package development tools, install dpkg-dev.",
     :depends ["kissapaketti"]},
    :python-minimal
    {:description "minimal subset of the Python language (default version)\nThis package contains the interpreter and some essential modules.  It's used\nin the boot process for some basic tasks.\nSee /usr/share/doc/python2.7-minimal/README.Debian for a list of the modules\ncontained in this package.",
     :depends ["python2.7-minimal" "dpkg"]}},
   :reverse
   {:binutils ["dpkg-dev"],
    :make ["dpkg-dev"],
    :patch ["dpkg-dev"],
    :bzip2 ["dpkg-dev"],
    :libdpkg-perl ["dpkg-dev"],
    :dpkg-dev [],
    :kissapaketti ["dpkg"],
    :python2.7-minimal ["python-minimal"],
    :xz-utils ["dpkg-dev"],
    :dpkg ["python-minimal"],
    :python-minimal [],
    :base-files ["dpkg-dev"]}})

(deftest dpkg-api-test
  (let [api (fixture/get-api)]
    (testing "parses file properly to json"
      (let [result (-> (mock/request :get (mk-uri))
                       api
                       parse-json-response)]
        (is (= expected-api-payload result))))))
