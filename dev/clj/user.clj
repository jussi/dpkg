(ns user
  (:require [integrant.repl :refer [clear prep init reset-all] :as i-repl]
            [config]
            [system]))

(i-repl/set-prep! #(config/get-system))

(defn go
  []
  (i-repl/go))

(defn reset
  []
  (i-repl/reset))

(defn halt
  []
  (i-repl/halt))
